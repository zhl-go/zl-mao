package events

import (
	"gitee.com/zhl-go/zl-mao/demo/app/modules/demo/controller/hooks"
	"gitee.com/zhl-go/zl-mao/demo/app/modules/demo/service"
	"gitee.com/zhl-go/zl-mao/frame"
)

type AfterCreateUserEvent struct {
}

// 钩子代号，必须对应
func (AfterCreateUserEvent) HookCode() string {
	return "beforeCreateUser"
}

// 事件处理方法
func (AfterCreateUserEvent) Handler(m *frame.Taskline, value frame.HookInterface) {
	m.Transaction(func() {
		arg := value.(*hooks.AfterCreateUserHook)
		userService := service.NewUserService(m)
		userService.UserDomain().Create(arg.UserName + "2")
		//try.Throw(-1, "踩踩踩")
	})
}

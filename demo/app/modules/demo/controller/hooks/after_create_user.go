package hooks

type AfterCreateUserHook struct {
	UserName string
}

// 钩子名称
func (AfterCreateUserHook) HookName() string {
	return "创建用户时"
}

// 钩子代号
func (AfterCreateUserHook) HookCode() string {
	return "beforeCreateUser"
}

package crontabs

import (
	"time"

	"gitee.com/zhl-go/zl-mao/frame"
)

type AutoCreateUserCrontab struct {
}

// 任务名称
func (this *AutoCreateUserCrontab) Name() string {
	return "自动创建用户"
}

// 定时时间表达式
func (this *AutoCreateUserCrontab) Schedule() string {
	return "0 */2 * * * *"
}

// 执行方法
func (this *AutoCreateUserCrontab) Handler(m *frame.Taskline) {
	m.LogDebug("这是一条警告日志！")
	m.LogDebugf("这是一条警告日志%d！", time.Now().Unix())
}

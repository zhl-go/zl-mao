package service

import (
	"gitee.com/zhl-go/zl-mao/demo/app/modules/demo/components/user"
	"gitee.com/zhl-go/zl-mao/frame"
)

type UserService struct {
	*user.Component
}

func NewUserService(t *frame.Taskline) *UserService {
	object := new(UserService)
	object.Component = user.New(t)
	return object
}

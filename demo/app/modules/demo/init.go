package demo

import (
	"fmt"

	"gitee.com/zhl-go/zl-mao/demo/app/modules/demo/controller/api"
	"gitee.com/zhl-go/zl-mao/demo/app/modules/demo/controller/crontabs"
	"gitee.com/zhl-go/zl-mao/demo/app/modules/demo/controller/events"
	"gitee.com/zhl-go/zl-mao/demo/app/modules/demo/controller/hooks"
	"gitee.com/zhl-go/zl-mao/demo/app/modules/demo/service"
	"gitee.com/zhl-go/zl-mao/frame"
)

type Module struct {
	frame.Module
}

func New() *Module {
	return &Module{}
}

// 执行安装操作
func (this *Module) Install() {

}

// 模块是否有效
func (this *Module) Enable() bool {
	return true
}

// 模块名称
func (this *Module) ModuleName() string {
	return "演示模块"
}

// 模块初始化，此处可放置路由
func (this *Module) Init(eg *frame.WebEngine) {
	eg.Use(func(w *frame.Webline) {
		defer func() {
			fmt.Println("您正在访问：", w.GetRouterName())
		}()
		w.Next()
	})
	userGroup := eg.Group("/user").Permission("用户管理", "1")
	{
		userGroup.GET("/user", api.UserController{}.Get).Permission("用户详情", "1.0")
		userGroup.POST("/user", api.UserController{}.Create).Permission("用户添加", "1.1")
		userGroup.PUT("/user", api.UserController{}.Update).Permission("用户修改", "1.2")
		userGroup.DELETE("/user", api.UserController{}.Remove).Permission("用户删除", "1.3")
		userGroup.GET("/users", api.UserController{}.Find).Permission("用户查询", "1.4")
		userGroup.GET("/config", api.UserController{}.Config).Permission("用户详情", "1.0")
	}
	sysGroup := eg.Group("/sys").Permission("系统管理", "2")
	{
		sysGroup.GET("/config", api.UserController{}.Config).Permission("获取配置结构", "2.1")
		sysGroup.POST("/config", api.UserController{}.Config).Permission("保存配置", "2.2")
		sysGroup.GET("/permission", api.UserController{}.GetPermision).Permission("查看权限", "2.3")
	}
}

// 定时任务
func (this *Module) Crontabs() []frame.CrontabInterface {
	return []frame.CrontabInterface{
		&crontabs.AutoCreateUserCrontab{},
	}
}

// 组件
func (this *Module) Components() []frame.ComponentInterface {
	return []frame.ComponentInterface{
		service.NewUserService(this.Taskline()),
	}
}

// 钩子列表（注册后的事件才能使用）
func (this *Module) Hooks() []frame.HookInterface {
	return []frame.HookInterface{
		&hooks.AfterCreateUserHook{},
	}
}

// 事件列表（自动寻找对应钩子）
func (this *Module) Events() []frame.EventInterface {
	return []frame.EventInterface{
		&events.AfterCreateUserEvent{},
	}
}

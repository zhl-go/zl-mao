package user

import (
	"gitee.com/zhl-go/zl-mao/frame"
)

type Component struct {
	*frame.Taskline
}

func New(t *frame.Taskline) *Component {
	object := new(Component)
	object.Taskline = t
	return object
}

// 组件名称
func (this *Component) CompName() string {
	return "用户"
}

// 数据库模型
func (this *Component) Models() []frame.ModelInterface {
	return []frame.ModelInterface{
		this.NewUser(),
	}
}

// 配置信息
func (this *Component) Config() frame.ConfigInterface {
	return this.RenderConfig(&Config{})
}

// 实例化user model
func (this *Component) NewUser() *User {
	userEntity := this.OrmModel(&User{}).(*User)
	userEntity.SetTableName("User100")
	return userEntity
}

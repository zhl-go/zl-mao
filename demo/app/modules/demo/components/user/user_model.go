package user

import (
	"time"

	"gitee.com/zhl-go/zl-mao/frame"
)

type User struct {
	Id          int       `xorm:"pk autoincr"`
	Username    string    `xorm:"varchar(255)"`
	CreatedAt   time.Time `xorm:"created"`
	frame.Model `xorm:"-"`
}

func (this *User) PrimaryKey() any {
	return this.Id
}

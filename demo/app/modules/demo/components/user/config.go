package user

import "gitee.com/zhl-go/zl-mao/frame"

type Config struct {
	Open    bool `name:"开启站点"`
	MaxUser int  `name:"最大用户数量"`
}

// 默认配置
func (this *Config) Default() frame.ConfigInterface {
	this.Open = true
	this.MaxUser = 32
	return this
}

// 请求前
func (this *Config) BeforeGet() {

}

// 配置名称
func (this *Config) ConfigName() string {
	return "用户设置"
}

// 配置别名
func (this *Config) ConfigAlias() string {
	return "user.set"
}

// 验证
func (this *Config) Validate() error {
	return nil
}

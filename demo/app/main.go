package main

import (
	"fmt"

	"gitee.com/zhl-go/zl-mao/demo/app/modules/demo"
	"gitee.com/zhl-go/zl-mao/frame"
)

func main() {
	sev := frame.NewServer(new(App))
	sev.Run()
}

type App struct {
}

// 程序启动时执行
func (this *App) Init(m *frame.WebEngine) {
}

// 配置文件
func (this *App) Config() any {
	return "./config.ini"
}

// 模块
func (this *App) Modules() []frame.ModuleInterface {
	return []frame.ModuleInterface{
		demo.New(),
	}
}

// 安装执行脚本
func (this *App) Install() error {
	return nil
}

// 系统版本
func (this *App) Version() string {
	return "1.0.0"
}

// 权限验证
func (this *App) CheckPermission(w *frame.Webline, code, name string) {
	fmt.Println(code, name)
}

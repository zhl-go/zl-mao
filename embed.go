package main

import (
	"bytes"
	"embed"
	"fmt"
	"io/ioutil"
	"os"
)

//go:embed demo/*
var demoEmbed embed.FS

// 拷贝文件
func copyDir(fromDir string, toDir string) {
	files, err := demoEmbed.ReadDir(fromDir)
	if err != nil {
		fmt.Println("目录"+fromDir+"打开失败：", err.Error())
		os.Exit(0)
		return
	}
	if err := os.Mkdir(toDir, 0755); err != nil {
		fmt.Println("目录"+toDir+"创建失败", err.Error())
		os.Exit(0)
		return
	}
	for _, item := range files {
		if item.Name() == "." || item.Name() == ".." {
			continue
		}
		if item.IsDir() {
			copyDir(fromDir+"/"+item.Name(), toDir+"/"+item.Name())
		} else {
			copyFile(fromDir+"/"+item.Name(), toDir+"/"+item.Name())
		}
	}
}

// 拷贝文件
func copyFile(fromFile string, toFile string) {
	data, err := demoEmbed.ReadFile(fromFile)
	if err != nil {
		fmt.Println("文件"+fromFile+"打开失败：", err.Error())
		os.Exit(0)
		return
	}
	//替换通用报名
	data = bytes.ReplaceAll(data, []byte("gitee.com/zhl-go/zl-mao/demo/app"), []byte(getModName()))
	if err := ioutil.WriteFile(toFile, data, 0660); err != nil {
		fmt.Println("文件"+toFile+"写入失败：", err.Error())
		os.Exit(0)
		return
	}
}

// 写入模型文件
func writeMod(name string) {
	var tpl = `module %s `
	tpl = fmt.Sprintf(tpl, name)
	err := ioutil.WriteFile(name+"/go.mod", []byte(tpl), 0766)
	if err != nil {
		fmt.Println("go.mod创建失败")
	}
}

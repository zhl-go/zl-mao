rm -rf demo/app/logs
rm -rf demo/app/runtime
rm -rf demo/app/config
echo "正在编译linux平台"
export GOOS=linux
export GOARCH=amd64
go build -o bin/mao &&
echo "linux平台编译成功"

echo "正在编译windows平台"
export GOOS=windows
export GOARCH=amd64
go build -o bin/mao.exe &&
echo "windows平台编译成功"

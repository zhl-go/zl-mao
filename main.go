package main

import (
	"fmt"
	"os"
	"strings"

	"gitee.com/zhl-go/zl-mao/frame"
)

func main() {
	args := os.Args[1:]
	if len(args) == 0 {
		fmt.Println("参数不能为空 ")
		return
	}
	argsLen := len(args)
	if args[0] == "new" {
		if argsLen < 3 {
			fmt.Println("请输入：mao 命令 参数... ")
			fmt.Println(`
	命令 new:
	子命令：
	app 创建一个web项目，例如：mao new app blog
	mod 创建一个web模块，例如：mao new mod pay ;(注意执行命令所在的目录)
	comop 创建一个web组件，例如：mao new comp user; (注意执行命令所在的目录)
			`)
			return
		}
		action, name := args[1], args[2]
		switch action {
		case "app":
			mod = name
			copyDir("demo/app", name)
			writeMod(name)
		case "module", "mod":
			copyDir("demo/app/modules/demo", name)
		case "comp", "component":
			copyDir("demo/app/modules/demo/components/user", name)
		}
	} else if args[0] == "version" {
		fmt.Println(frame.VERSION)
	} else {
		fmt.Println("参数错误")
		return
	}
}

var mod string

// 获取当前应用的mod名称
func getModName() string {
	if mod != "" {
		return mod
	}
	workPath := getWorPath()
	for index, item := range workPath {
		if item == "modules" {
			mod = workPath[index-1]
			return mod
		}
	}
	fmt.Println("请确认所在目录是否为web项目")
	os.Exit(0)
	return ""
}

func getWorPath() []string {
	workDir, _ := os.Getwd()
	workDir = strings.ReplaceAll(workDir, `\`, "/")
	workDir = strings.TrimSuffix(workDir, "/")
	return strings.Split(workDir, "/")
}

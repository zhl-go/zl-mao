package try

type Exception interface {
	ErrCode() int32
	ErrMsg() string
}

type BaseException struct {
	code int32
	msg  string
}

// 错误代号
func (this *BaseException) ErrCode() int32 {
	return this.code
}

// 错误消息
func (this *BaseException) ErrMsg() string {
	return this.msg
}

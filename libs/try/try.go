package try

import "fmt"

type CatchFunc func(e Exception)

// 执行
func Do(fn func(), cfn ...CatchFunc) {
	defer func() {
		err := getException(recover())
		if len(cfn) > 0 && err != nil {
			cfn[0](err)
		}
	}()
	fn()
}

// 执行协程
func Go(fn func(), efn ...CatchFunc) {
	go Do(fn, efn...)
}

// 将recover获取的错误封装成Exception
func getException(err any) Exception {
	if err == nil {
		return nil
	}
	switch e := err.(type) {
	case Exception:
		return e
	default:
		return &BaseException{code: -1, msg: fmt.Sprint(err)}
	}
}

package try

import (
	"testing"
	"time"
)

func TestTry(t *testing.T) {
	t.Log("测试开始")
	Do(func() {
		t.Log("执行程序")
		Throw(100, "错误001")
	}, func(e Exception) {
		t.Log("异常抛出：", e.ErrCode(), e.ErrMsg())
	})
	Go(func() {
		t.Log("执行异步程序")
		Throw(100, "错误002")
	}, func(e Exception) {
		t.Log("异步异常抛出：", e.ErrCode(), e.ErrMsg())
	})
	time.Sleep(time.Second * 1)
}

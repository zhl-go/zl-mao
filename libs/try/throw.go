package try

import "fmt"

// 抛出异常
func Throw(code int32, args ...any) {
	panic(&BaseException{code: code, msg: fmt.Sprint(args...)})
}

// 抛出异常并格式化
func Throwf(code int32, format string, args ...any) {
	panic(&BaseException{code: code, msg: fmt.Sprintf(format, args...)})
}

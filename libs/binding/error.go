package binding

// 绑定错误
type BindError struct {
	field string //错误字段
	msg   string //提示消息
	err   error  //错误内容
}

func (this *BindError) Field() string {
	return this.field
}

func (this *BindError) Msg() string {
	return this.msg
}

func (this *BindError) Error() string {
	if this.err != nil {
		return this.err.Error()
	}
	return ""
}

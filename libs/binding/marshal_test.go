package binding

import (
	"encoding/json"
	"fmt"
	"testing"

	"github.com/gin-gonic/gin"
)

type B string

func (this B) MarshalJSON() ([]byte, error) {
	return []byte(`18`), nil
}

func TestMarshal(t *testing.T) {
	type Role struct {
		Name string `json:"name"`
	}
	type Info struct {
		Name string  `json:"name"`
		Role []*Role `json:"role"`
		Age  B
		M    gin.H
	}
	s := Marshal(Info{Name: "zhangsan", M: gin.H{"王": 1}})
	fmt.Println(s)
}

//
const testTimes = 2000

type testData struct {
	Name string   `json:"name"`
	Sons []string `json:"name"`
}

var data = testData{
	Name: "王xx",
	Sons: []string{"王老大", "王老二"},
}

func Benchmark_json(b *testing.B) {
	for i := 0; i < testTimes; i++ { //use b.N for looping
		_, _ = json.Marshal(data)
	}
}

func Benchmark_binding(b *testing.B) {
	for i := 0; i < testTimes; i++ { //use b.N for looping
		_ = Marshal(data)
	}
}

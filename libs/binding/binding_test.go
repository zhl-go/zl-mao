package binding

import (
	"encoding/json"
	"fmt"
	"reflect"
	"testing"
	"time"

	"github.com/gin-gonic/gin"
)

type Extend struct {
	High int `bind:"high" valid:"gt=100:身高不能低于1米 lt=200:身高不能高于2米"`
}

type User struct {
	///Ids       []int  `bind:"ids" binding:"len:长度不足 age:大小不足 len=1,max=2,min=3,gt=2,lt=3,gte=3,lte=3,required:高度不足"`
	//Name string `bind:"name" valid:"in=wan|wht:姓名错误 len=3:姓名必须为3位  min=3,max=12:用户名为6-12个字符"`
	Age       int       `bind:"age" valid:"required:宝宝"`
	Date      time.Time `bind:"date" time-format:"2006-01-02"`
	ChengNian bool      `bind:"chengnian"`
	Email     string    `bind:"email" valid:"email:请输入正确的邮箱地址"`
	Son       Userb     `bind:"son"`
	//Friends []Userb `bind:"friends" valid:"len=2:至少有两个好朋友吧"`
	Extend
}

type Userb struct {
	Name string `bind:"name" valid:"required:用户名不能为空 min=6,max=12:用户名为6-12个字符"`
	Age  int    `bind:"age"`
	C    Userc  `bind:"c"`
}

type Userc struct {
	Name string `bind:"name"`
	Age  int    `bind:"age"`
}

func TestBindMap(t *testing.T) {
	m := map[string]interface{}{
		"name":      "wht",
		"age":       30,
		"date":      "2012-11-11",
		"chengnian": "true",
		"high":      200,
		"email":     "haitgo@qq.com",
		"ids":       []interface{}{1, 2, 3, 4},
		"son": map[string]interface{}{
			"name": "王舒瑶123",
			"age":  10,
			"c": map[string]interface{}{
				"name": "王舒涵",
				"age":  5,
			},
		},
		"friends": []interface{}{
			map[string]interface{}{
				"name": "黄勇",
				"age":  22,
			},
		},
	}
	user := new(User)
	//m = make(map[string]interface{})
	b := Bind(user, m)
	t.Log(user)
	if err := b.Error(); err != nil {
		t.Fatal("绑定失败-------:", err.Error(), err.Error(), b.err.msg, b.err.field)
	}
	tm, _ := time.Parse("2006-01-02", "2012-11-11")
	t.Log("解析", tm)
	t.Log("时间", user.Date.Format("2006-01-02"))
	bt, _ := json.Marshal(user)
	t.Log(string(bt))

	mm := []int{1, 2, 3, 4}
	bbb, _ := json.Marshal(mm)
	var mmm interface{}
	json.Unmarshal(bbb, &mmm)
	t.Log("输出：", mmm, "类型：", reflect.TypeOf(mmm).Kind().String())
	switch mmm.(type) {
	case []int:
		t.Log("类型[]int")
	case []int64:
		t.Log("类型[]int64")
	case []interface{}:
		t.Log("类型[]interface")
	}
}

func TestBindFloat(t *testing.T) {
	var obj struct {
		Money int64 `bind:"money" valid:""`
	}
	b := Bind(&obj, gin.H{
		"money": 32,
	})
	fmt.Println("绑定结果", obj)
	if err := b.Error(); err != nil {
		t.Fatal("绑定失败:", err.Error(), err.Error(), b.err.msg, b.err.field)
	}
}

func TestMoreTitle(t *testing.T) {
	var obj struct {
		Money int64 `bind:"money" valid:"required->不能为空"`
	}
	b := Bind(&obj, gin.H{
		//"money": 1,
	})
	fmt.Println("绑定结果", obj)
	if err := b.Error(); err != nil {
		t.Fatal("绑定失败:", err.Error(), err.Error(), b.err.msg, b.err.field)
	}
}

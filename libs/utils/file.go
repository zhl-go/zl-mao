package utils

import "os"

// 判断文件是否存在
func FileExists(filepath string) bool {
	f, e := os.Open(filepath)
	if f != nil {
		defer f.Close()
	}
	return e == nil
}

package utils

import "reflect"

// 结构体名称
func StructName(obj interface{}) string {
	t := reflect.TypeOf(obj)
	if t.Kind() == reflect.Ptr {
		t = t.Elem()
	}
	return t.Name()
}

// 变量名
func VariableName(v interface{}) string {
	vt := reflect.ValueOf(v)
	if vt.Kind() == reflect.Ptr {
		vt = vt.Elem()
	}
	return ""
}

package utils

import (
	"fmt"
	"strconv"
)

// 浮点数取小数位数
func FloatFiexd(n float64, l int) float64 {
	d := fmt.Sprintf(fmt.Sprint("%.", l, "f"), n)
	v, _ := strconv.ParseFloat(d, 64)
	return v
}

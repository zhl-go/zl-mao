package utils

// 数组包含
func ArrayIn(item interface{}, datas ...interface{}) bool {
	for _, data := range datas {
		if item == data {
			return true
		}
	}
	return false
}

// 数组不包含
func ArrayNotIn(item interface{}, datas ...interface{}) bool {
	for _, data := range datas {
		if item == data {
			return false
		}
	}
	return true
}

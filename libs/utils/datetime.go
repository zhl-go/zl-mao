package utils

import (
	"time"
)

const (
	FOMRAT_DATE      = "2006-01-02"
	FORMAT_DATE_TIME = "2006-01-02 15:04:05"
)

// 获取当前时间的星期一时间
func GetMonday(t time.Time) time.Time {
	m := map[string]int{
		"Monday":    0,
		"Tuesday":   -1,
		"Wednesday": -2,
		"Thursday":  -3,
		"Friday":    -4,
		"Saturday":  -5,
		"Sunday":    -6,
	}
	day := m[t.Weekday().String()]
	return t.AddDate(0, 0, day)
}

// 当前日期
func Date() string {
	return time.Now().Format(FOMRAT_DATE)
}

// 当前日期时间
func DateTime() string {
	return time.Now().Format(FORMAT_DATE_TIME)
}

package utils

import (
	"os"
	"testing"
)

func TestZip(t *testing.T) {
	files := make([]*os.File, 0)
	f1, _ := os.Open("../types")
	f2, _ := os.Open("../orm")
	files = append(files, f1, f2)
	ZipCompress(files, "./test.zip")
}

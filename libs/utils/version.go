package utils

import (
	"fmt"
	"strconv"
	"strings"
)

// 版本号转换为数字
// 1.1.1转换后为100010001
func VersionToNumber(version string) int64 {
	arr := strings.Split(version, ".")
	var n = ""
	for _, d := range arr {
		n += fmt.Sprintf("%04s", d)
	}
	d, _ := strconv.ParseInt(n, 10, 64)
	return d
}

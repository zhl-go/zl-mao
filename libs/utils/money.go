package utils

import (
	"github.com/shopspring/decimal"
)

// 分转换为元
func MoneyFenToString(f int64) string {
	d := decimal.NewFromInt(f)
	return d.Div(decimal.NewFromInt(100)).String()
}

// 将分输出成为 元
type MoneyFen int64

func (d MoneyFen) MarshalJSON() ([]byte, error) {
	s := MoneyFenToString(int64(d))
	return []byte(s), nil
}

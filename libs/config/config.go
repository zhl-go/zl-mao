// 配置文件的读取和写入
// 目前仅支持json格式配置文件
package config

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"os"
)

// 配置读取,只读取json类型文件,可读到多个结构体上,如果配置不存在则创建一个
func Read(filename string, ptr interface{}, ptrs ...interface{}) error {
	if _, err := os.Open(filename); err != nil {
		return Write(filename, ptr)
	}
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		return err
	}
	ptrs = append(ptrs, ptr)
	for _, ptr := range ptrs {
		if ptr == nil {
			continue
		}
		if err := json.Unmarshal(data, ptr); err != nil {
			return err
		}
	}
	return nil
}

// 配置写入,写入前先将json数据格式化，方便修改时易读
func Write(filename string, ptr interface{}) error {
	data, err := json.Marshal(ptr)
	if err != nil {
		return err
	}
	var str bytes.Buffer
	if err = json.Indent(&str, []byte(data), "", "    "); err != nil {
		return err
	}
	return ioutil.WriteFile(filename, str.Bytes(), 0600)
}

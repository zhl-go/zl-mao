package crontab

import (
	"fmt"
	"testing"
	"time"
)

func TestToDO(t *testing.T) {
	tasks := New()
	job, err := NewJobModel("*/3 * * * * *", func() {
		fmt.Println(time.Now().Format("2006-01-02 15:04:05"), "哈哈")
	})
	tasks.DynamicRegister("aaa", job)
	fmt.Println(err)
	time.Sleep(time.Second * 30)
}

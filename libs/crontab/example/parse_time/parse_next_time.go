package main

import (
	"fmt"
	"time"

	cronlib "gitee.com/zhl-go/zl-mao/libs/crontab"
)

func main() {
	t, err := cronlib.Parse("0 0 0 */1 * *")
	if err != nil {
		panic(err.Error())
	}

	fmt.Println(" now: ", time.Now())
	next := t.Next(time.Now())
	fmt.Println("next: ", next)
}

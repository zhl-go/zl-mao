package frame

import (
	"sync"
)

type hookDomain struct {
	sync.Mutex
	server    *Server
	eventsMap map[string][]EventInterface
}

// 初始化事件驾照
func (this *hookDomain) init(module ModuleInterface) {
	if this.eventsMap == nil {
		this.eventsMap = make(map[string][]EventInterface)
	}
	events := module.Events()
	for _, event := range events {
		key := event.HookCode()
		m, ok := this.eventsMap[key]
		if !ok {
			this.eventsMap[key] = make([]EventInterface, 0)
			m = make([]EventInterface, 0)
		}
		this.eventsMap[key] = append(m, event)
	}
}

// 触发钩子
func (this *hookDomain) trigger(tl *Taskline, h HookInterface) {
	this.Lock()
	events, ok := this.eventsMap[h.HookCode()]
	this.Unlock()
	if !ok {
		return
	}
	for _, event := range events {
		event.Handler(tl, h)
	}
}

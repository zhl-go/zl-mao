package frame

const (
	VERSION = "1.0.24"
)
const (
	DIR_CONFIG  = "./config"
	DIR_RUNTIME = "./runtime"
	DIR_LOGS    = "./logs"
)

// 错误代号[600以下的保存到日志，600以上的不保存]
const (
	code_nil   = 0   //空错误
	CODE_ERROR = 510 //错误但不致命
	CODE_SQL   = 511 //sql错误
	CODE_FATAL = 512 //致命错误
	CODE_WARN  = 600 //错误警告3
)

package frame

import "xorm.io/xorm"

/*
	定义各种接口
*/

// 应用接口
type AppInterface interface {
	Init(*WebEngine)                                     //站点初始化执行方法(可以放一些中间件处理的东西)
	Config() any                                         //配置文件路径
	Modules() []ModuleInterface                          //加载需要的模块
	Install() error                                      //安装入口
	CheckPermission(webline *Webline, code, name string) //权限验证
	Version() string                                     //当前版本号
}

// 不启动web服务
type NoWebInterface interface {
	NoWeb()
}

// 模块接口
type ModuleInterface interface {
	Install()                         //执行安装操作
	ModuleName() string               //模块名称
	Enable() bool                     //是否启用，为false时自动限制接口访问
	Init(*WebEngine)                  //模块初始化，此处可放置路由
	Crontabs() []CrontabInterface     //定时任务
	Components() []ComponentInterface //组件
	Hooks() []HookInterface           //钩子列表（注册后的事件才能使用）
	Events() []EventInterface         //事件列表（自动寻找对应钩子）
}

// 组件接口
type ComponentInterface interface {
	CompName() string         //组件名称
	Models() []ModelInterface //数据库模型
}

// 模型接口
type ModelInterface interface {
	TableName() string
	PrimaryKey() any     //主键
	SetTableName(string) //设置表名
	init(sess *xorm.Session, obj ModelInterface)
}

type Configer interface {
	Config() ConfigInterface
}

// 配置接口
type ConfigInterface interface {
	Default() ConfigInterface //默认配置
	BeforeGet()               //请求前
	ConfigName() string       //配置名称
	ConfigAlias() string      //配置别名
	Validate() error          //验证
}

// 定时任务接口
type CrontabInterface interface {
	Name() string      //任务名称
	Schedule() string  //定时时间表达式
	Handler(*Taskline) //执行方法
}

// 钩子接口
type HookInterface interface {
	HookName() string //钩子名称
	HookCode() string //钩子代号
}

// 事件接口
type EventInterface interface {
	HookCode() string                       //钩子代号，必须对应
	Handler(m *Taskline, arg HookInterface) //事件处理方法
}

// 日志处理器接口
type Logger interface {
	Save()
}

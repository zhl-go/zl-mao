package frame

// 运行模式
type RunMode string

// 运行模式
const (
	RUN_MODE_DEVELOP RunMode = "dev" //开发者模式下会输入日志内容
	RUN_MODE_PRODUCT RunMode = "prod"
)

// 查询结构
type BaseQuery struct {
	Page  int `bind:"page" form:"page"`
	Limit int `bind:"limit" form:"limit"`
}

// 任务线处理器
type SetSetTaskLiner interface {
	SetTaskline(*Taskline)
}

// 任务线包装，用给模块组合使用
type Module struct {
	tl *Taskline
}

func (this *Module) Taskline() *Taskline {
	return this.tl
}
func (this *Module) SetTaskline(t *Taskline) {
	this.tl = t
}
